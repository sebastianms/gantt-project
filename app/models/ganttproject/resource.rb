module Ganttproject
  class Resource < ActiveRecord::Base
    has_many :assignments, dependent: :destroy
    has_many :tasks, through: :assignments
    belongs_to :project
  end
end
