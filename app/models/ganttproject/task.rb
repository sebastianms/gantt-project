module Ganttproject
  class Task < ActiveRecord::Base
    has_ancestry
    has_many :assignments, dependent: :destroy
    has_many :resources, through: :assignments
    belongs_to :project, :foreign_key => "project_id"
    def from_params(params, id)
      self.name       = params["#{id}_text"].to_s
      self.start      = params["#{id}_start_date"].to_date
      self.finish      = params["#{id}_end_date"].to_date
      self.duration   = params["#{id}_duration"].to_i
      self.progress   = params["#{id}_progress"].to_i
      if params["#{id}_parent"].to_i>0
        self.parent_id     = params["#{id}_parent"].to_i
      end
      self.project_id=params[:id]

    end

    def full_name
      (self.ancestors.where("ancestry is not ?",nil).map(&:name)+ [self.name]).join(" > ")
    end
    def parent_name
      if self.parent 
        self.parent.name + " - "
      end
    end
    def calculate_values
      if self.is_childless?
        self.childless = 1
      else
        self.childless = 0
      end
      self.full_name = self.parent_parent_name + self.parent_name + self.name
    end
    def parent_parent_name
      if self.parent && self.parent.parent
        self.parent.parent.name + " - "
      end
    end
    def calculate_ponderators
      begin
        if self.has_children?
          self.children.to_a.sum{|c| c.calculate_ponderators}
        else
          ponderator
        end
      rescue => error
        0
      end
    end
    def calculate_ponderated_progress
      begin
        if self.has_children?
          self.children.to_a.sum{|c| c.calculate_ponderated_progress}
        else
          ponderated_progress
        end
      rescue => error
        0
      end
    end
    def update_progress
      self.calculate_progress
      self.save
      if self.parent
        self.parent.update_progress
      else
        self.store_progress
      end
    end
    def ponderated_progress
      tasks = ::Task.where(master_unique_id: self.unique_id, sector_id: self.sector_id)
      if tasks.any?
        tasks.to_a.sum{|t| t.progress*(t.end_at.to_time.to_i - t.start_at.to_time.to_i)}
      else
        0
      end
    end
    def ponderator
      tasks = ::Task.where(master_unique_id: self.unique_id, sector_id: self.sector_id)
      tasks_sum = 0
      time_sum = self.finish.to_time.to_i - self.start.to_time.to_i
      if tasks.any?
        tasks_sum = tasks.to_a.sum{|t| (t.end_at.to_time.to_i - t.start_at.to_time.to_i)}
      end
      if time_sum >= tasks_sum
        time_sum
      else
        tasks_sum
      end
    end
    def calculate_progress
      a = calculate_ponderators
      b = calculate_ponderated_progress
      a!=0 ? self.progress = (b/a.to_f).round(2) : self.progress = 0
    end

    def total_vs_released_constraints
      tasks = ::Task.where(master_unique_id: self.unique_id, sector_id: self.sector_id)
      constraints = ::Constraint.unscoped.where("(task_id in (?) or master_task_id = ?) and sector_id = ?", tasks.ids, self.unique_id, self.sector_id).group(:completed).count
      released = constraints[true] || 0
      total = (constraints[false] || 0) + released
      [released, total]
    end

    def expected_progress(now=DateTime.now.to_time.to_i)
      start_time = self.start.to_time.to_i
      end_time = self.finish.to_time.to_i
      expected_progress = (((now-start_time)/(end_time - start_time).to_f).round(2))*100
      if expected_progress < 0
        0
      elsif expected_progress > 100
        100
      else
        expected_progress
      end
    end
    def state
      threshold = 3
      if (expected_progress - threshold) > self.progress
        "red"
      elsif expected_progress > self.progress && self.progress >= (expected_progress - threshold)
        "yellow"
      elsif expected_progress <= self.progress && self.progress < (expected_progress + threshold)
        "green"
      elsif (expected_progress + threshold) >= self.progress
        "blue"
      else
        "white"
      end
    end

    def store_progress
      project = self.project
      historical = Historical.create(sector_id: project.sector_id, progress: self.progress,
                                     expected_progress: self.expected_progress, project_id: project.id, task_id: self.id)
    end

  end
end
