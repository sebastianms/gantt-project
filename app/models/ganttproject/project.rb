module Ganttproject
  class Project < ActiveRecord::Base
    has_paper_trail# :ignore => [:file_name]
    has_many :tasks , dependent: :destroy
    has_many :resources, dependent: :destroy
    has_many :links, dependent: :destroy

    attr_accessor :file
    before_save :change_filename_and_upload_to_s3 if :file
    after_commit :process_project_file , if: :file

    def change_filename_and_upload_to_s3
      self.file_name =  file.original_filename
      s3_url = AwsConnection.new.upload_file_to_s3(self.file_name,file.path)
      self.file_path = s3_url
    end

    def process_project_file
      ::Project.import_from_msproject(self.id, self.file_path, self.file_name)
    end

    def import(filepath,filename)
      t1 = Time.now
      input_file = open(filepath)

      split_name = filename.split(".")
      output_file = Tempfile.new(["tmp-#{split_name.first}", ".#{split_name.last}"])

      output_file.binmode
      output_file.write input_file.read
      output_file.flush

      output_file.seek(0)

      project = MPXJ::Reader.read(output_file.path)
      project_id=self.id
      #self.update_attribute(:file_name,filename)

      resource_id_hash = Hash.new
      task_id_hash = Hash.new

      logger.info("There are #{project.all_resources.size} resources in this project")
      logger.info("First resource: #{project.all_resources.first}")
      project.all_resources.each do |resource|
        r=Ganttproject::Resource.new
        r.project_id=project_id
        r.initials=resource.initials
        r.max_units=resource.max_units
        r.cost=resource.cost
        r.save

        resource_id_hash[resource.id]=r.id
      end


      logger.info("There are #{project.all_tasks.size} tasks in this project")
      task = project.all_tasks.first
      #logger.info("First Task: #{task.inspect}")
      # logger.info("First Task...")
      # logger.info("ID: #{task.id}")
      # logger.info("name: #{task.name}")
      # logger.info("unique id: #{task.unique_id}")
      # logger.info("status: #{task.status}")
      # logger.info("start: #{task.start}")
      # logger.info("stop: #{task.stop}")
      # logger.info("finish: #{task.finish}")
      # logger.info("progress: #{task.progress}")
      # logger.info("GUID: #{task.guid}")
      # logger.info("Child tasks N: #{project.child_tasks.size}")
      project.child_tasks.each do |childTask|
        recursive_task_creation(childTask,nil,project_id,task_id_hash)
      end


      logger.info("There are #{project.all_assignments.size} assignments in this project")
      logger.info("First Assigment: #{project.all_assignments.first}")
      project.all_assignments.each do |assignment|
        a=Assignment.new
        a.resource_id=resource_id_hash[assignment.resource.id] if assignment.resource
        a.task_id=task_id_hash[assignment.task.id] if assignment.task
        a.start=assignment.start
        a.finish=assignment.finish
        a.save
      end

      logger.info("Checking links...")
      project.all_tasks.each do |task|
        task.successors.each do |link|
          #logger.info("Link: #{link.guid}")
          l=Link.new
          l.gtype = link.type
          l.source = task_id_hash[task.id]
          l.target = task_id_hash[project.get_task_by_unique_id(link.task_unique_id).id]
          l.project_id=project_id
          case link.type
            when "FS"
              l.gtype = 0
            when "SS"
              l.gtype = 1
            when "FF"
              l.gtype = 2
            when "SF"
              l.gtype = 3
            else
              l.gtype = 0
          end
          l.save
        end


      end

      t2 = Time.now
      logger.info("Time execution: #{t2-t1} segs")

      # self.file = nil
      # update file_name: filename

    end


    def recursive_task_creation(task,task_id,project_id,task_id_hash)

      task.child_tasks.each do |childTask|
        t = Ganttproject::Task.find_or_initialize_by(unique_id: childTask.unique_id, sector_id: sector_id)
        t.name = childTask.name
        t.project_id = project_id
        t.parent_id = task_id
        t.start = childTask.start
        t.finish = childTask.finish
        t.duration = (Date.parse(childTask.finish.to_s) - Date.parse(childTask.start.to_s)).to_i
        t.progress = 0
        t.childless = childTask.child_tasks.any? ? 0 : 1
        t.full_name = task.name + " - " + childTask.name
        t.guid = childTask.guid
        t.save

        task_id_hash[childTask.id] = t.id
        recursive_task_creation(childTask,t.id,project_id,task_id_hash) if childTask.child_tasks
      end

    end

  end
end
