module Ganttproject
  module TasksHelper

    def nested_tasks(tasks)
      tasks.map do |task, sub_task|
        render(task) + content_tag(:div, nested_tasks(sub_task), :class => "nested_tasks")
      end.join.html_safe
    end

  end
end
