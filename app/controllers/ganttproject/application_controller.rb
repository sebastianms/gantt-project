module Ganttproject
  class ApplicationController < ActionController::Base
    protect_from_forgery except: :index
    before_filter :verify_session
    include Rails.application.helpers

    def verify_session
    	render nothing: true, status: :unauthorized if current_user.nil?
    end
  end
end
