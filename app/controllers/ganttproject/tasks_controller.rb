require_dependency "ganttproject/application_controller"

module Ganttproject
  class TasksController < ApplicationController
    before_action :set_task, only: [:show, :edit, :update, :destroy]
    before_action :get_project

    def get_project
      @project = Project.find(params[:project_id])
    end

    # GET /tasks
    # GET /tasks.json
    def index
      @tasks = @project.tasks.all
    end

    # GET /tasks/1
    # GET /tasks/1.json
    def show
      @task = @project.tasks.find(params[:id])
    end

    # GET /tasks/new
    def new
      @task = @project.tasks.new(:parent_id => params[:parent_id])
    end

    # GET /tasks/1/edit
    def edit
    end

    # POST /tasks
    def create
      @task = Task.new(task_params)

      if @task.save
        redirect_to @task, notice: 'Task was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /tasks/1
    def update
      if @task.update(task_params)
        redirect_to @task, notice: 'Task was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /tasks/1
    def destroy
      @task.destroy
      redirect_to tasks_url, notice: 'Task was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_task
        @task = @project.tasks.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def task_params
        params.require(:task).permit(:name, :start, :finish, :duration, :progress, :parent_id, :project_id)
      end
  end
end
