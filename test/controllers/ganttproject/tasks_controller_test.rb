require 'test_helper'

module Ganttproject
  class TasksControllerTest < ActionController::TestCase
    setup do
      @task = ganttproject_tasks(:one)
      @routes = Engine.routes
    end

    test "should get index" do
      get :index
      assert_response :success
      assert_not_nil assigns(:tasks)
    end

    test "should get new" do
      get :new
      assert_response :success
    end

    test "should create task" do
      assert_difference('Task.count') do
        post :create, task: { duration: @task.duration, finish: @task.finish, name: @task.name, parent_id: @task.parent_id, progress: @task.progress, project_id: @task.project_id, start: @task.start }
      end

      assert_redirected_to task_path(assigns(:task))
    end

    test "should show task" do
      get :show, id: @task
      assert_response :success
    end

    test "should get edit" do
      get :edit, id: @task
      assert_response :success
    end

    test "should update task" do
      patch :update, id: @task, task: { duration: @task.duration, finish: @task.finish, name: @task.name, parent_id: @task.parent_id, progress: @task.progress, project_id: @task.project_id, start: @task.start }
      assert_redirected_to task_path(assigns(:task))
    end

    test "should destroy task" do
      assert_difference('Task.count', -1) do
        delete :destroy, id: @task
      end

      assert_redirected_to tasks_path
    end
  end
end
