class AddChildlessToTasks < ActiveRecord::Migration
  def change
    add_column :ganttproject_tasks, :childless, :integer, default: -1
  end
end
