class AddUniqueIdToGanttprojectTasks < ActiveRecord::Migration
  def change
    add_column :ganttproject_tasks, :unique_id, :integer
  end
end
