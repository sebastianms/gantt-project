class CreateGanttprojectLinks < ActiveRecord::Migration
  def change
    create_table :ganttproject_links do |t|
      t.integer :source
      t.integer :target
      t.integer :gtype
      t.integer :project_id

      t.timestamps null: false
    end
  end
end
