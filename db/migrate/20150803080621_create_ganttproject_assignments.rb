class CreateGanttprojectAssignments < ActiveRecord::Migration
  def change
    create_table :ganttproject_assignments do |t|
      t.integer :task_id
      t.integer :resource_id
      t.date :start
      t.date :finish

      t.timestamps null: false
    end
  end
end
